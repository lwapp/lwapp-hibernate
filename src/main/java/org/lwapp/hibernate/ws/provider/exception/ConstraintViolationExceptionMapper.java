package org.lwapp.hibernate.ws.provider.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.lwapp.commons.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
	private static final Logger LOG = LoggerFactory.getLogger(ConstraintViolationExceptionMapper.class);

	@Override
	public Response toResponse(final ConstraintViolationException ex) {
		LOG.error("ConstraintViolationException occured: {}", ExceptionUtils.getRootCauseMessage(ex));
		final ErrorResponse errorResponse = new ErrorResponse.Builder()//
				.errorCode("DUP.ERR")//
				.errorMessage("Duplicate operation request error. " + ExceptionUtils.getRootCauseMessage(ex.getCause()))//
				.build();
		return Response.status(Response.Status.BAD_REQUEST).entity(errorResponse).build();
	}

}
