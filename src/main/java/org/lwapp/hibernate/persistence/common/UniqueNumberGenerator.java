package org.lwapp.hibernate.persistence.common;

import java.math.BigInteger;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionFactoryImpl;
import org.lwapp.hibernate.persistence.util.HibernateUtils;

public class UniqueNumberGenerator {

	protected final SessionFactory sessionFactory;

	public UniqueNumberGenerator() {
		this.sessionFactory = HibernateUtils.getSessionFactory();
	}

	public long getUniqueNumber() {
		final SessionFactoryImpl ss = (SessionFactoryImpl) sessionFactory;
		final String sql = ss.getDialect().getSequenceNextValString("UUID_SEQ");
		final SQLQuery q = sessionFactory.getCurrentSession().createSQLQuery(sql);
		final long value = ((BigInteger) q.uniqueResult()).longValue();
		return value + System.nanoTime();
	}

	public String getUniqueString() {
		final SessionFactoryImpl ss = (SessionFactoryImpl) sessionFactory;
		final String sql = ss.getDialect().getSequenceNextValString("UUID_SEQ");
		final SQLQuery q = sessionFactory.getCurrentSession().createSQLQuery(sql);
		final long value = ((BigInteger) q.uniqueResult()).longValue();
		return UUID.randomUUID().toString().replaceAll("-", "") + value;
	}

	public String generateUniqueApiKey() {
		return DigestUtils.sha256Hex(getUniqueString());
	}

	public String generateUniqueApiSecret() {
		return DigestUtils.sha512Hex(getUniqueString());
	}

}
