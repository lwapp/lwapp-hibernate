package org.lwapp.hibernate.persistence.common;

public class InvalidFingerPrintException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidFingerPrintException(final String message) {
        super(message);
    }
}
