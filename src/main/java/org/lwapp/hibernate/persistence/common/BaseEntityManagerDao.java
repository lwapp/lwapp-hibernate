package org.lwapp.hibernate.persistence.common;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang3.Validate;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.internal.SessionFactoryImpl;

import com.google.common.base.Preconditions;

public abstract class BaseEntityManagerDao<T extends AbstractEntity> {

    @PersistenceUnit
    protected EntityManager entityManager;
    protected final Class<?> entityClass;

    public BaseEntityManagerDao() {
        this.entityClass = Generics.getTypeParameter(getClass());
    }

    public Session currentSession() {
        return entityManager.unwrap(Session.class);
    }

    protected Criteria criteria() {
        return criteria(currentSession());
    }

    protected Query getNamedQuery(final String namedQuery) {
        return getNamedQuery(currentSession(), namedQuery);
    }

    protected Query createQuery(final String queryString) {
        return createQuery(currentSession(), queryString);
    }

    protected Query createSQLQuery(final String queryString) {
        return createSQLQuery(currentSession(), queryString);
    }

    protected Criteria criteria(final Session session) {
        return session.createCriteria(entityClass);
    }

    protected Query getNamedQuery(final Session session, final String namedQuery) {
        return session.getNamedQuery(namedQuery);
    }

    protected Query createQuery(final Session session, final String queryString) {
        return session.createQuery(queryString);
    }

    protected Query createSQLQuery(final Session session, final String queryString) {
        return session.createSQLQuery(queryString);
    }

    public T persist(final T entity) {
        entity.validate();
        final Session currentSession = currentSession();
        updateFingerPrint(entity);
        currentSession.save(Preconditions.checkNotNull(entity));
        return entity;
    }

    protected Long generateUniqueId() {
        final SessionFactoryImpl ss = entityManager.unwrap(SessionFactoryImpl.class);
        final String sql = ss.getDialect().getSequenceNextValString("ID_SEQ");
        final SQLQuery q = currentSession().createSQLQuery(sql);
        return ((BigInteger) q.uniqueResult()).longValue();
    }

    public void update(final T entity) {
        Preconditions.checkNotNull(entity);
        Preconditions.checkNotNull(((AbstractEntity) entity).getId());
        ((AbstractEntity) entity).validate();
        final Session currentSession = currentSession();
        updateFingerPrint(entity);
        currentSession.update(entity);
    }

    @SuppressWarnings("unchecked")
    public static <R extends AbstractEntity> void recalculateFingerPrint(final long id, final String entityClassName, final Session session) {
        Validate.notBlank(entityClassName, "Entity class name is mandatory.");
        final R entity = (R) session.get(entityClassName, Preconditions.checkNotNull(id));
        Preconditions.checkNotNull(entity);
        Preconditions.checkNotNull(((AbstractEntity) entity).getId());
        ((AbstractEntity) entity).validate();
        updateFingerPrint(entity);
        session.update(entity);
    }

    public void deleteById(final long id) {
        final Session session = currentSession();
        session.delete(findById(id));
    }

    @SuppressWarnings("unchecked")
    public T findById(final Serializable id) {
        final T entity = (T) currentSession().get(entityClass, Preconditions.checkNotNull(id));
        validateFingerPrint(entity);
        return entity;
    }

    @SuppressWarnings("unchecked")
    public T uniqueResult(final Criteria criteria) {
        final T result = (T) criteria.uniqueResult();
        validateFingerPrint(result);
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<T> list(final Criteria criteria) {
        final List<T> results = criteria.list();
        for (final T entity : results) {
            validateFingerPrint(entity);
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    public List<T> listAll() {
        final List<T> results = criteria().list();
        for (final T entity : results) {
            validateFingerPrint(entity);
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    public T uniqueResult(final Query query) {
        final T result = (T) query.uniqueResult();
        validateFingerPrint(result);
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<T> list(final Query query) {
        final List<T> results = query.list();
        for (final T entity : results) {
            validateFingerPrint(entity);
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    public T uniqueResult(final SQLQuery query) {
        final T result = (T) query.uniqueResult();
        validateFingerPrint(result);
        return result;
    }

    @SuppressWarnings("unchecked")
    public List<T> list(final SQLQuery query) {
        final List<T> results = query.list();
        for (final T entity : results) {
            validateFingerPrint(entity);
        }
        return results;
    }

    private static void updateFingerPrint(final AbstractEntity entity) {
        if (entity.getClass().isAnnotationPresent(Fingerprintable.class)) {
            ((FingerprintableBaseEntity) entity).setFingerPrint(((FingerprintableBaseEntity) entity).generateFingerPrint());
        }
    }

    // Override if dont want to validate the fingerprint
    protected void validateFingerPrint(final T entity) {
        if ((entity != null) && entity.getClass().isAnnotationPresent(Fingerprintable.class)) {
            final FingerprintableBaseEntity fe = (FingerprintableBaseEntity) entity;
            if (!fe.generateFingerPrint().equals(fe.getFingerPrint())) {
                throw new InvalidFingerPrintException(
                        "Fingerprint mismatch for the given id:" + entity.getId() + ". Make sure the fingerprint value is not altered outside of the application.");
            }
        }
    }

}
