package org.lwapp.hibernate.persistence.common;

public abstract class ReadOnlyBaseEntityDao<T extends AbstractEntity> extends AbstractEntityDao<T> {

	@Override
	public T persist(final T entity) {
		throw new UnsupportedOperationException("Create entity not supported for the " + entity.getClass());
	}

	@Override
	public void update(final T entity) {
		throw new UnsupportedOperationException("Update entity not supported for the " + entity.getClass());
	}

}
