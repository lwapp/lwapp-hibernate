package org.lwapp.hibernate.persistence.common;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.Session;
import org.jboss.weld.exceptions.IllegalArgumentException;
import org.lwapp.commons.cli.AbstractCommand;
import org.lwapp.commons.cli.Terminal;
import org.lwapp.hibernate.persistence.util.HibernateUtils;
import org.lwapp.hibernate.persistence.util.TransactionHelper;

public class DatabaseCommand extends AbstractCommand {

	public DatabaseCommand() {
		super("db");
	}

	@Override
	public void printDescription(final Terminal term) {
		term.println("Manage application registration and its configurations.");
	}

	@Override
	public void printUsage(final Terminal term) {
		term.println("'db -refingerprint'");
	}

	@Override
	public void execute(final Terminal terminal, final String arguments) throws Exception {

		try {
			final Session currentSession = HibernateUtils.getSessionFactory().getCurrentSession();
			final String response = TransactionHelper.executeInTransaction(currentSession, () -> {
				if (StringUtils.isBlank(arguments)) {
					throw new IllegalArgumentException("Bad command.");
				}

				final String[] args = arguments.trim().split(" ");

				if (args.length != 2) {
					throw new IllegalArgumentException("Bad command.");
				}

				if ("-refingerprint".equalsIgnoreCase(args[0])) {
					final String id = terminal.readString("Please enter the database id.");
					final String entityClassName = terminal.readString("Please enter the complete entity class name. i.e com.example.Employee");
					AbstractEntityDao.recalculateFingerPrint(Long.valueOf(id), entityClassName, currentSession);
					return String.format("\n\nFingerprint recalculation succussfull.");

				} else {
					throw new IllegalArgumentException("Bad command.");
				}
			});

			terminal.println(response);
		} catch (final Exception e) {
			terminal.println(ExceptionUtils.getStackTrace(e));
			printUsage(terminal);
		}
	}

}
