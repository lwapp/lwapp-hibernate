package org.lwapp.hibernate.persistence.common;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.reflections.ReflectionUtils;

@MappedSuperclass
@Fingerprintable
public abstract class FingerprintableBaseEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;
	@Column(nullable = false)
	private String fingerPrint;
	@Column(nullable = false)
	private int fingerPrintVersion;

	public FingerprintableBaseEntity() {
		this(1);
	}

	public FingerprintableBaseEntity(final int fingerPrintVersion) {
		this.fingerPrintVersion = fingerPrintVersion;
	}

	private String getFingerprintKey() {
		return String.valueOf(Math.abs(String.valueOf(getId()).hashCode()));
	}

	final String generateFingerPrint() {
		final StringBuilder sb = getFingerPrintableString();
		return DigestUtils.sha512Hex(sb.toString());
	}

	private StringBuilder getFingerPrintableString() {
		final StringBuilder sb = new StringBuilder();
		for (final String value : getSortedFingerprintableValues()) {
			if (value != null) {
				sb.append(value);
			}
		}
		return sb;
	}

	private List<String> getSortedFingerprintableValues() {
		final List<String> fingerPrintableValues = new ArrayList<>();

		fingerPrintableValues.add(String.valueOf(getId()));
		fingerPrintableValues.add(String.valueOf(fingerPrintVersion));
		fingerPrintableValues.add(getFingerprintKey());

		for (final Object value : getFingerprintableValues()) {
			if (value != null) {
				final String formattedValue;
				if (value instanceof Date) {
					formattedValue = DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.format((Date) value);
				} else {
					formattedValue = String.valueOf(value);
				}

				fingerPrintableValues.add(formattedValue);
			}
		}

		// Sort the values to avoid unnecessary ordering issues while
		// calculating the hash.
		Collections.sort(fingerPrintableValues);
		return Collections.unmodifiableList(fingerPrintableValues);
	}

	String getFingerPrint() {
		return fingerPrint;
	}

	void setFingerPrint(final String fingerPrint) {
		this.fingerPrint = fingerPrint;
	}

	int getFingerPrintVersion() {
		return fingerPrintVersion;
	}

	protected void setFingerPrintVersion(final int fingerPrintVersion) {
		this.fingerPrintVersion = fingerPrintVersion;
	}

	// This is a private because we don't want to expose sensitive information.
	@SuppressWarnings("unchecked")
	private List<Object> getFingerprintableValues() {
		final List<Object> fieldValues = new ArrayList<>();
		try {
			final Set<Field> fields = ReflectionUtils.getFields(getClass());
			for (final Field field : fields) {
				field.setAccessible(true);
				final Object value = field.get(this);
				if ((value != null) && !isAnnotationPresent(field, IgnoreFingerprint.class, Version.class, Transient.class)) {
					fieldValues.add(value);
				}
			}
			return fieldValues;
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	private boolean isAnnotationPresent(final Field field, final Class<? extends Annotation>... annotations) {

		if (annotations != null) {
			for (final Class<? extends Annotation> annotationClass : annotations) {
				if (field.isAnnotationPresent(annotationClass)) {
					return true;
				}
			}
		}

		return false;
	}

}
