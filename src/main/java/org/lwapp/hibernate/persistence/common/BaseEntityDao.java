package org.lwapp.hibernate.persistence.common;

public abstract class BaseEntityDao<T extends BaseEntity> extends AbstractEntityDao<T> {

	@Override
	public T persist(final T entity) {
		entity.setId(generateUniqueId());
		return super.persist(entity);
	}

}
