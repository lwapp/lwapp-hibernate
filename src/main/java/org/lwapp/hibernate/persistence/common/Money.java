package org.lwapp.hibernate.persistence.common;

import java.io.Serializable;
import java.util.Currency;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.apache.commons.lang3.Validate;

@Embeddable
public final class Money implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private Long amount;
    @Column(nullable = false)
    private String currency;

    public Money() {
    }

    public Money(final long amount, final String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public void validate() throws IllegalArgumentException {
        Validate.isTrue(((amount != null)), "Money value is mandatory.");
        Validate.notNull(Currency.getInstance(currency), "Money currency is mandatory and must be a valid currency. Actual currency =" + currency);
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(final Long value) {
        this.amount = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final Currency currency) {
        this.currency = currency.getCurrencyCode();
    }

    public void setCurrency(final String currency) {
        setCurrency(Currency.getInstance(currency));
    }

    @Override
    public String toString() {
        return amount + currency;
    }

}
