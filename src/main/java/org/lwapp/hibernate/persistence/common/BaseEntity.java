package org.lwapp.hibernate.persistence.common;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public abstract class BaseEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    @Id
    private Long id;
    @Version
    @IgnoreFingerprint
    private long version;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    void setVersion(final long version) {
        this.version = version;
    }

}
