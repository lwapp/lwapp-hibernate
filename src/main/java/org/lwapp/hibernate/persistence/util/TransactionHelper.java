package org.lwapp.hibernate.persistence.util;

import java.util.function.Supplier;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class TransactionHelper {

	private static final Logger LOG = LoggerFactory.getLogger(HibernateUtils.class);

	/**
	 * Begins a hibernate {@link Transaction} associated with the current
	 * session.
	 * 
	 * @param session Hibernate session
	 * @return a {@link Transaction} instance
	 */
	public static Transaction beginTransaction(final Session session) {
		return session.beginTransaction();
	}

	/**
	 * Commit an active {@link Transaction} associated with the current session.
	 * This method can be used to commit a unit of database operations in a
	 * given transaction context. Upon any exception the transaction will be
	 * rolled back.
	 * 
	 * @param txn Hibernate transaction
	 *
	 */
	public static void commitTransaction(final Transaction txn) {
		if ((txn != null) && (txn.getStatus() == TransactionStatus.ACTIVE)) {
			txn.commit();
		}
	}

	/**
	 * Executes a {@link Supplier} inside a transaction.
	 * 
	 * @param <R> the type of results supplied to the supplier
	 * @param session Hibernate session
	 * @param supplier see {@link Supplier}
	 * @return the Result
	 */
	public static <R> R executeInTransaction(final Session session, final Supplier<R> supplier) {
		Transaction txn = null;
		try {
			txn = beginTransaction(session);
			final R result = supplier.get();
			commitTransaction(txn);
			return result;
		} catch (final Exception e) {
			LOG.error("Exception occured while performing the operation. Transaction will be rollbacked.", e);
			rollbackTransaction(txn);
			throw e;
		}
	}

	/**
	 * Executes a {@link Supplier} inside a transaction. Session is created
	 * using getCurrentSession()
	 * 
	 * @param <R> the type of results supplied to the supplier
	 * @param supplier see {@link Supplier}
	 * @return the Result
	 */
	public static <R> R executeInTransaction(final Supplier<R> supplier) {
		final Session session = HibernateUtils.getSessionFactory().getCurrentSession();
		return executeInTransaction(session, supplier);
	}

	/**
	 * Rollback an active {@link Transaction} associated with the current
	 * session. This method can be used in case of any exception occurred while
	 * performing a database operation in a given transaction context.
	 *
	 * @param txn Hibernate transaction
	 */
	public static void rollbackTransaction(final Transaction txn) {
		if ((txn != null) && (txn.getStatus() == TransactionStatus.ACTIVE)) {
			txn.rollback();
		}
	}

}
