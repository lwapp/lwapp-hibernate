package org.lwapp.hibernate.persistence.util;

import java.util.Set;

import javax.persistence.Entity;

import org.apache.commons.lang3.Validate;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.lwapp.commons.utils.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class HibernateUtils {

    private static final Logger LOG = LoggerFactory.getLogger(HibernateUtils.class);

    private static SessionFactory sessionFactory;

    public static void buildSessionFactory(final Configuration hibernateConfiguration, final String... packages) {
        Validate.notEmpty(packages, "Please provide packages to load.");

        final Set<Class<?>> entityClasses = ClassUtils.getAllClassesWithAnnotation(Entity.class);

        LOG.info("Building hibernate session factory. {} Entity classes will be registered.", entityClasses.size());
        for (final Class<?> clazz : entityClasses) {
            LOG.info(clazz.getName());
            hibernateConfiguration.addAnnotatedClass(clazz);
        }

        final StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder().applySettings(hibernateConfiguration.getProperties());
        sessionFactory = hibernateConfiguration.buildSessionFactory(standardServiceRegistryBuilder.build());
        LOG.info("Session factory successfully created.");
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
