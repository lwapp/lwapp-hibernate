package org.lwapp.hibernate.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.lwapp.hibernate.persistence.util.HibernateUtils;
import org.lwapp.hibernate.persistence.util.TransactionHelper;

@Interceptor
public class UnitOfWorkInterceptor {

	@AroundInvoke
	public Object aroundInvoke(final InvocationContext context) throws Exception {
		UnitOfWork unitOfWork = null;
		Session session = null;
		Transaction txn = null;
		try {
			unitOfWork = context.getMethod().getAnnotation(UnitOfWork.class);
			if (unitOfWork != null) {
				session = HibernateUtils.getSessionFactory().getCurrentSession();
				txn = session.beginTransaction();
				session.setDefaultReadOnly(unitOfWork.readOnly());
				session.setCacheMode(CacheMode.valueOf(unitOfWork.cacheMode().name()));
			}
			final Object response = context.proceed();
			if (unitOfWork != null) {
				TransactionHelper.commitTransaction(txn);
			}
			return response;
		} catch (final Exception e) {
			if (unitOfWork != null) {
				TransactionHelper.rollbackTransaction(txn);
			}
			throw e;
		}
	}
}
