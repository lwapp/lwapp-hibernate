package org.lwapp.hibernate.interceptor;

public enum CacheMode {
    NORMAL, IGNORE, GET, PUT, REFRESH;
}