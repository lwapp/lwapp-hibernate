package org.lwapp.hibernate.interceptor;


import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


@Target(METHOD)
@Retention(RUNTIME)
@Documented
public @interface UnitOfWork {
    boolean readOnly() default false;

    CacheMode cacheMode() default CacheMode.NORMAL;

}
