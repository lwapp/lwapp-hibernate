package org.lwapp.hibernate.config;

import org.lwapp.configclient.Configurable;
import org.lwapp.configclient.Configuration;

public enum LwappConfigurations implements Configurable {

    DB_URL("application.db.url", false, true), //
    DB_USERNAME("application.db.username", false, true), //
    DB_PASSWORD("application.db.password", true, true), //
    ;

    private final Configuration configuration;

    private LwappConfigurations(final String propertyName, final boolean secure, final boolean mandatory) {
        configuration = getConfiguration(propertyName, secure, mandatory);
    }

    @Override
    public Configuration getConfiguration() {
        return configuration;
    }

}
