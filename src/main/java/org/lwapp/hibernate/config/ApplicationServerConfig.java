package org.lwapp.hibernate.config;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.lwapp.configclient.Configuration;
import org.lwapp.configclient.client.ConfigurationServiceClient;

@Singleton
public class ApplicationServerConfig {

    @Inject
    private ConfigurationServiceClient configurationServiceClient;

    public String getDbUrl() {
        return configurationServiceClient.getString(LwappConfigurations.DB_URL);
    }

    public String[] getApplicationPackagesToLoad() {
        return configurationServiceClient.getApplicationPackagesToLoad();
    }

    public String getDbUserName() {
        return configurationServiceClient.getString(LwappConfigurations.DB_USERNAME);
    }

    public byte[] getDbPassword() {
        return configurationServiceClient.getByteArray(LwappConfigurations.DB_PASSWORD);
    }

    public void updateDbPassword(final byte[] dbPassword) {
        final Configuration configuration = LwappConfigurations.DB_PASSWORD.getConfiguration();
        configuration.setPropertyValue(dbPassword);
    }

    public void updateDbUserName(final String dbUsername) {
        final Configuration configuration = LwappConfigurations.DB_USERNAME.getConfiguration();
        configuration.setPropertyValue(dbUsername.getBytes());
    }

}
