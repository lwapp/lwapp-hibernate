
package org.lwapp.hibernate;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;
import org.lwapp.commons.cli.Terminal;
import org.lwapp.configclient.AutoStartable;
import org.lwapp.hibernate.config.ApplicationServerConfig;
import org.lwapp.hibernate.persistence.util.HibernateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class HibernateConfigurator implements AutoStartable {

    private static final Logger LOG = LoggerFactory.getLogger(HibernateConfigurator.class);

    @Inject
    private ApplicationServerConfig applicationServerConfig;

    @Override
    public void init() {
        final Terminal terminal = new Terminal();

        final String dbUrl = applicationServerConfig.getDbUrl();
        Validate.notBlank(dbUrl, "Please provide dbUrl in application properties or configuration settings.");
        final String[] packages = applicationServerConfig.getApplicationPackagesToLoad();
        Validate.notEmpty(packages, "Please provide packages to load.");

        String dbUsername = applicationServerConfig.getDbUserName();
        if (StringUtils.isBlank(dbUsername)) {
            dbUsername = terminal.readString("Please enter db username:");
            applicationServerConfig.updateDbUserName(dbUsername);
        }

        byte[] dbPassword = applicationServerConfig.getDbPassword();
        if (dbPassword == null) {
            dbPassword = terminal.readPassword("Please enter db password:").getBytes();
            applicationServerConfig.updateDbPassword(dbPassword);
        }

        final Configuration hibernateConfiguration = new Configuration().configure("hibernate.cfg.xml");
        hibernateConfiguration.setProperty(AvailableSettings.URL, dbUrl);
        hibernateConfiguration.setProperty(AvailableSettings.USER, dbUsername);
        hibernateConfiguration.setProperty(AvailableSettings.PASS, new String(dbPassword));

        // ******To load all the tables in the database******//
        final String isInstallFirstTime = terminal.readString("Do you want to install for the first time? [YeS/No]");
        LOG.info("isInstallFirstTime:'{}'", StringUtils.defaultString(isInstallFirstTime, "No"));
        if ("YeS".equals(isInstallFirstTime)) {
            System.out.println("Please execute the following SQL statements.");
            LOG.info("********************************************");
            System.out.println("CREATE SEQUENCE ID_SEQ;");
            LOG.info("********************************************");
            final String rOrU = terminal.readString("Do you want to recreate[R] or Update[U] the database tables? [R/U]");
            if ("R".equals(rOrU)) {
                hibernateConfiguration.setProperty(AvailableSettings.HBM2DDL_AUTO, "create");
            } else if ("U".equals(rOrU)) {
                hibernateConfiguration.setProperty(AvailableSettings.HBM2DDL_AUTO, "update");
            }

        } else {
            hibernateConfiguration.setProperty(AvailableSettings.HBM2DDL_AUTO, "");
        }

        HibernateUtils.buildSessionFactory(hibernateConfiguration, packages);
    }

}
