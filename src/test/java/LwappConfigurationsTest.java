import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.lwapp.configclient.ConfigUtils;
import org.lwapp.configclient.Configuration;

public class LwappConfigurationsTest {

    @Test
    public void testConfigurations() {
        final Set<Configuration> configurations = ConfigUtils.findConfigurations();
        Assert.assertEquals(7, configurations.size());
    }
}
